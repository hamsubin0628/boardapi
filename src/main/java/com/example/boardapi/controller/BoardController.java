package com.example.boardapi.controller;

import com.example.boardapi.model.*;
import com.example.boardapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/user")
    public String setBoard(@RequestBody BoardRequest request){
        boardService.setBoard(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<BoardItem> getBoards(){
        return boardService.getBoards();
    }

    @GetMapping("/detail/{id}")
    public BoardResponse getBoard(@PathVariable long id){
        return boardService.getBoard(id);
    }

    @PutMapping("/board-info/{id}")
    public String putBoardInfo(@PathVariable long id, @RequestBody BoardInfoChangeRequest request){
        boardService.putBoardInfo(id, request);

        return "OK";
    }
    @PutMapping("/user-info/{id}")
    public String putBoardUserInfo(@PathVariable long id, @RequestBody BoardUserInfoChaneRequest request){
        boardService.putBoardUserInfo(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delBoard(@PathVariable long id){
        boardService.delBoard(id);

        return "OK";
    }
}
