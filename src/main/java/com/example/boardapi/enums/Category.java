package com.example.boardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    DAILY("일상"),
    HUMOR("유머"),
    LOVE("연애"),
    STUDY("공부");

    private final String categoryEnum;

}
