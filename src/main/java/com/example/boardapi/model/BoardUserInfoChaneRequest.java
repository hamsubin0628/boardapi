package com.example.boardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardUserInfoChaneRequest {
    private String userId;
    private String userPassWord;
    private String userName;
}
