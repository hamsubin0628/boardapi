package com.example.boardapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardResponse {
    private Long id;
    private LocalDate createDate;
    private String categoryName;
    private String boardName;
    private String boardTitle;
    private String boardContents;
    private String boardComment;
}
