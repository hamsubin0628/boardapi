package com.example.boardapi.model;

import com.example.boardapi.enums.Category;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardRequest {
    private String userId;

    private String userPassWord;

    private String userName;

    private LocalDate createDate;

    @Enumerated(value = EnumType.STRING)
    private Category category;

    private String boardName;

    private String boardTitle;

    private String boardContents;

    private String boardComment;
}
