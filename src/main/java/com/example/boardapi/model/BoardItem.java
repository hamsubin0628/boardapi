package com.example.boardapi.model;

import com.example.boardapi.enums.Category;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardItem {
    private Long id;
    private LocalDate createDate;
    private String categoryName;
    private String boardName;
    private String boardTitle;
    private String boardContents;
    private String boardComment;
}
