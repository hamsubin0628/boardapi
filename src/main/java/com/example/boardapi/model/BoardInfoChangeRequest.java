package com.example.boardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardInfoChangeRequest {
    private String boardTitle;
    private String boardContents;
    private String boardComment;
}
