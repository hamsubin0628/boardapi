package com.example.boardapi.service;

import com.example.boardapi.entity.Board;
import com.example.boardapi.model.*;
import com.example.boardapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard(BoardRequest request){
        Board addDate = new Board();
        addDate.setUserId(request.getUserId());
        addDate.setUserPassWord(request.getUserPassWord());
        addDate.setUserName(request.getUserName());
        addDate.setBoardName(request.getBoardName());
        addDate.setCreateDate(LocalDate.now());
        addDate.setCategory(request.getCategory());
        addDate.setBoardName(request.getBoardName());
        addDate.setBoardTitle(request.getBoardTitle());
        addDate.setBoardContents(request.getBoardContents());
        addDate.setBoardComment(request.getBoardComment());

        boardRepository.save(addDate);
    }

    public List<BoardItem> getBoards(){
        List<Board> originList = boardRepository.findAll();

        List<BoardItem> result = new LinkedList<>();

        for(Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setCreateDate(board.getCreateDate());
            addItem.setCategoryName(board.getCategory().getCategoryEnum());
            addItem.setBoardName(board.getBoardName());
            addItem.setBoardTitle(board.getBoardTitle());
            addItem.setBoardContents(board.getBoardContents());
            addItem.setBoardComment(board.getBoardComment());

            result.add(addItem);
        }
        return result;
    }

    public BoardResponse getBoard(long id){
        Board originData = boardRepository.findById(id).orElseThrow();

        BoardResponse response = new BoardResponse();
        response.setId(originData.getId());
        response.setCreateDate(originData.getCreateDate());
        response.setCategoryName(originData.getCategory().getCategoryEnum());
        response.setBoardName(originData.getBoardName());
        response.setBoardTitle(originData.getBoardTitle());
        response.setBoardContents(originData.getBoardContents());
        response.setBoardComment(originData.getBoardComment());

        return response;
    }

    public void putBoardInfo(long id, BoardInfoChangeRequest request){
        Board originData = boardRepository.findById(id).orElseThrow();
        originData.setBoardTitle(request.getBoardTitle());
        originData.setBoardContents(request.getBoardContents());
        originData.setBoardComment(request.getBoardComment());

        boardRepository.save(originData);
    }
    public void putBoardUserInfo(long id, BoardUserInfoChaneRequest request){
        Board originData = boardRepository.findById(id).orElseThrow();
        originData.setUserId(request.getUserId());
        originData.setUserPassWord(request.getUserPassWord());
        originData.setUserName(request.getUserName());

        boardRepository.save(originData);
    }

    public void delBoard(long id){boardRepository.deleteById(id);}
}
